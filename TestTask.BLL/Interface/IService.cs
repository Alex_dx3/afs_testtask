﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.BLL.Infrastructure;
using TestTask.BLL.DTO;

namespace TestTask.BLL.Interface
{
    public interface IService<T> : IDisposable
        where T : class
    {
        OperationDetails Create(T item);
        T Get(int id);
        List<T> GetList();
        OperationDetails Update(T item);
        OperationDetails Delete(int id);
    }
}
