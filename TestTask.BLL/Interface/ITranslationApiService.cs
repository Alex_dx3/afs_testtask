﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.BLL.Interface
{
    public interface ITranslationApiService
    {
        Task<(string, bool)> GetTranslatedText(string text, string url);
    }
}
