﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.BLL.DTO;
using TestTask.BLL.Infrastructure;

namespace TestTask.BLL.Interface
{
    public interface IRequestService : IService<TranslationRequestDTO>
    {
        List<TranslationRequestDTO> GetList(int TranslationId);
        Task<OperationDetails> ExecuteRequestAsync(TranslationRequestDTO toTranslate);
    }
}
