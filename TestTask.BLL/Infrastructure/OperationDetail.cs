﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.BLL.Infrastructure
{
    public struct PropertyMessage
    {
        public string Message { get; set; }
        public string Property { get; set; }
        public PropertyMessage(string prop, string message)
        {
            Message = message;
            Property = prop;
        }
    }
    public class OperationDetails
    {
        public OperationDetails()
        {
            Succedeed = true;
            Messages = new List<PropertyMessage>();
        }
        public OperationDetails(bool succedeed, List<PropertyMessage> propMessage)
        {
            Succedeed = succedeed;
            Messages = propMessage;
        }
        public bool Succedeed { get; set; }
        public List<PropertyMessage> Messages { get; set; }
    }
}
