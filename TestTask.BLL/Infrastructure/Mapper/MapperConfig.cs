﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using TestTask.DAL.Entities;
using TestTask.BLL.DTO;

namespace TestTask.BLL.Infrastructure
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<TranslationTypeDTO, TranslationType>();
            CreateMap<TranslationRequestDTO, TranslationRequest>();
        }
    }
}
