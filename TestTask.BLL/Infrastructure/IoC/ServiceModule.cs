﻿using Autofac;
using AutoMapper;
using TestTask.BLL.Interface;
using TestTask.BLL.Services;
using TestTask.DAL.Interfaces;
using TestTask.DAL.Repositories;

namespace TestTask.BLL.Infrastructure
{
    public class ServiceModule : Module
    {
        public string connectionString;

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new UnitOfWork(connectionString)).As<IUnitOfWork>();
            builder.Register(c => new FuntranslationApiService()).As<ITranslationApiService>();
            builder.Register(c => new TranslationRequestService(c.Resolve<IUnitOfWork>(), c.Resolve<IMapper>(), c.Resolve<ITranslationApiService>())).As<IRequestService>();
            builder.Register(c => new TranslationTypeService(c.Resolve<IUnitOfWork>(), c.Resolve<IMapper>())).As<ITranslationService>();
        }
    }
}