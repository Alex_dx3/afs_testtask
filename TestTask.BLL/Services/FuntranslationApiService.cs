﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TestTask.BLL.DTO;
using TestTask.BLL.Interface;

namespace TestTask.BLL.Services
{
    public class FuntranslationApiService : ITranslationApiService
    {
        public async Task<(string, bool)> GetTranslatedText(string text, string url)
        {
            try
            {
                using (var client = new HttpClient())
                {

                    var uri = new Uri(url);


                    var requestContent = new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("text", text),
                });

                    var response = await client.PostAsync(
                        uri,
                        requestContent);

                    var resultString = response.Content.ReadAsStringAsync().Result;

                    FuntranslationsResponseDTO result = JsonConvert.DeserializeObject<FuntranslationsResponseDTO>(resultString);

                    if (result.success != null)
                    {
                        return (result.contents.translated, true);
                    }
                }

            }
            catch (UriFormatException e)
            {
                return ("Wrong url", false);
            }


            return ("Service not avaliable", false);


        }
    }
}
