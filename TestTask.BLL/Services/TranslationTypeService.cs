﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.BLL.Infrastructure;
using TestTask.BLL.Interface;
using TestTask.DAL.Entities;
using TestTask.DAL.Interfaces;
using TestTask.BLL.DTO;
using AutoMapper;

namespace TestTask.BLL.Services
{
    public class TranslationTypeService : ITranslationService
    {
        private IUnitOfWork Database { get; set; }
        private IMapper Mapper { get; set; }
        public TranslationTypeService(IUnitOfWork uow, IMapper mapper)
        {
            Database = uow;
            Mapper = mapper;
        }
        public OperationDetails Create(TranslationTypeDTO translationTypeDTO)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {

        }

        public OperationDetails Update(TranslationTypeDTO itemDTO)
        {
            throw new NotImplementedException();
        }



        public OperationDetails Delete(int id)
        {
            throw new NotImplementedException();
        }



        public TranslationTypeDTO Get(int id)
        {
            var transaction = Database.TranslationRepository.GetById(id);
            var transactionDTO = Mapper.Map<TranslationTypeDTO>(transaction);
            return transactionDTO;
        }

        public List<TranslationTypeDTO> GetList()
        {
            var transactions = Database.TranslationRepository.GetAll();
            var transactionsDTO = Mapper.Map<List<TranslationTypeDTO>>(transactions);
            return transactionsDTO;
        }
    }
}
