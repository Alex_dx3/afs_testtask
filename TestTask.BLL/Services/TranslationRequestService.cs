﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.BLL.Infrastructure;
using TestTask.BLL.Interface;
using TestTask.DAL.Entities;
using TestTask.DAL.Interfaces;
using TestTask.BLL.DTO;
using AutoMapper;
using System.Net.Http;
using Newtonsoft.Json;

namespace TestTask.BLL.Services
{
    public class TranslationRequestService : IRequestService
    {
        private IUnitOfWork Database { get; set; }
        private IMapper Mapper { get; set; }
        private ITranslationApiService ApiService { get; set; }
        public TranslationRequestService(IUnitOfWork uow, IMapper mapper, ITranslationApiService apiService)
        {
            Database = uow;
            Mapper = mapper;
            ApiService = apiService;
        }
        public OperationDetails Create(TranslationRequestDTO requestDTO)
        {
            var operationDetail = new OperationDetails();

            if (String.IsNullOrEmpty(requestDTO.Text))
            {
                operationDetail.Succedeed = false;
                operationDetail.Messages.Add(new PropertyMessage("Text", "Text to translate is empty"));
            }
            if (String.IsNullOrEmpty(requestDTO.ResultText))
            {
                operationDetail.Succedeed = false;
                operationDetail.Messages.Add(new PropertyMessage("ResultText", "Translated text is empty"));
            }
            var translatorType = Database.TranslationRepository.GetById(requestDTO.TranslationId);
            if (translatorType == null)
            {
                operationDetail.Succedeed = false;
                operationDetail.Messages.Add(new PropertyMessage("TranslationId", "Translation type isn't correct"));
            }
            if (operationDetail.Succedeed)
            {
                var request = Mapper.Map<TranslationRequest>(requestDTO);
                Database.RequestRepository.Save(request);
                Database.SaveAsync();
            }
            return operationDetail;
        }

        public void Dispose()
        {

        }

        public OperationDetails Update(TranslationRequestDTO itemDTO)
        {
            throw new NotImplementedException();
        }



        public OperationDetails Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<TranslationRequestDTO> GetList(int TranslationId)
        {
            var requests = Database.RequestRepository.GetAll(t => t.TranslationId == TranslationId);
            var requestsDTO = Mapper.Map<IQueryable<TranslationRequestDTO>>(requests);
            return requestsDTO.ToList();
        }

        public TranslationRequestDTO Get(int id)
        {
            var request = Database.RequestRepository.GetById(id);
            var requestDTO = Mapper.Map<TranslationRequestDTO>(request);
            return requestDTO;
        }

        public List<TranslationRequestDTO> GetList()
        {
            var requests = Database.RequestRepository.GetAll();
            var requestsDTO = Mapper.Map<IQueryable<TranslationRequestDTO>>(requests);
            return requestsDTO.ToList();
        }

        public async Task<OperationDetails> ExecuteRequestAsync(TranslationRequestDTO request)
        {
            OperationDetails operationDetail = new OperationDetails();
            if (String.IsNullOrEmpty(request.Text))
            {
                operationDetail.Succedeed = false;
                operationDetail.Messages.Add(new PropertyMessage("Text", "Text to translate is empty"));
            }

            var translatorType = Database.TranslationRepository.GetById(request.TranslationId);
            if (translatorType == null)
            {
                operationDetail.Succedeed = false;
                operationDetail.Messages.Add(new PropertyMessage("TranslationId", "Translation type isn't correct"));
            }

            if (operationDetail.Succedeed)
            {
                var result = await ApiService.GetTranslatedText(request.Text, translatorType.Url);
                if (!result.Item2)
                {
                    operationDetail.Succedeed = false;
                    operationDetail.Messages.Add(new PropertyMessage("ResultText", result.Item1));
                }
                else
                {
                    request.ResultText = result.Item1;
                }
            }
            return operationDetail;
        }


    }
}
