﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.BLL.DTO
{
    public class FuntranslationsResponseDTO
    {
        public SuccessPart success { get; set; }
        public ContentsPart contents { get; set; }
    }

    public class SuccessPart
    {
        public int total { get; set; }
    }

    public class ContentsPart
    {
        public string translation { get; set; }
        public string text { get; set; }
        public string translated { get; set; }
    }
}
