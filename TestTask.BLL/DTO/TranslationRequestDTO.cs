﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.BLL.DTO
{
    public class TranslationRequestDTO
    {
        public int Id { get; set; }
        public int TranslationId { get; set; }
        public string Text { get; set; }
        public string ResultText { get; set; }
    }
}
