﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TestTask.BLL.DTO;
using TestTask.BLL.Interface;
using TestTask.BLL.Services;
using TestTask.DAL.Interfaces;

namespace TestTask.Tests.BLL
{
    [TestClass]
    public class TranslationRequestServiceTest
    {
        [TestMethod]
        public async Task ExecuteRequestAsyncTest()
        {
            var uowMock = new Mock<IUnitOfWork>();
            uowMock.Setup(a => a.TranslationRepository.GetById(1)).Returns(new DAL.Entities.TranslationType { Name = "testname1", Id = 1, Url = "http://api.funtranslations.com/translate/yoda.json" });
            uowMock.Setup(a => a.TranslationRepository.GetById(2)).Returns<DAL.Entities.TranslationType>(null);
            var apiMock = new Mock<ITranslationApiService>();
            apiMock.Setup(a => a.GetTranslatedText("Master Obiwan has lost a planet.", "http://api.funtranslations.com/translate/yoda.json")).Returns(Task.FromResult(("Lost a planet,  master obiwan has. ", true)));

            TranslationRequestService service = new TranslationRequestService(uowMock.Object, null, apiMock.Object);
            var testRequest1 = new TranslationRequestDTO { Text = "Master Obiwan has lost a planet.", TranslationId = 1 };
            var testRequest2 = new TranslationRequestDTO { Text = "Master Obiwan has lost a planet.", TranslationId = 2 };

            var result1 = await service.ExecuteRequestAsync(testRequest1);
            var result2 = await service.ExecuteRequestAsync(testRequest2);

            Assert.AreEqual(true, result1.Succedeed);
            Assert.AreEqual(false, result2.Succedeed);
            Assert.AreEqual("Lost a planet,  master obiwan has. ", testRequest1.ResultText);
        }
    }
}
