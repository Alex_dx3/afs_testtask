﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestTask.BLL.Services;
namespace TestTask.Tests.BLL
{
    [TestClass]
    public class FutranslationsApiServiceTest
    {
        [TestMethod]
        public async Task GetTranslatedTextCorrectTest()
        {
            var service = new FuntranslationApiService();

            var result = await service.GetTranslatedText("Master Obiwan has lost a planet.", "http://api.funtranslations.com/translate/yoda.json");

            Assert.AreEqual(true, result.Item2);
            Assert.AreEqual("Lost a planet,  master obiwan has. ", result.Item1);
        }

        [TestMethod]
        public async Task GetTranslatedTextWrongUrlTest()
        {
            var service = new FuntranslationApiService();

            var result = await service.GetTranslatedText("Master Obiwan has lost a planet.", "wrong url");

            Assert.AreEqual(false, result.Item2);
            Assert.AreEqual("Wrong url", result.Item1);
        }
    }
}
