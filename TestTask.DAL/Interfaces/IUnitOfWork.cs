﻿using System;
using System.Threading.Tasks;
using TestTask.DAL.Repositories;
using TestTask.DAL.Entities;

namespace TestTask.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TranslationType> TranslationRepository { get; }
        IRepository<TranslationRequest> RequestRepository { get; }
        Task SaveAsync();
    }
}
