﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TestTask.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T GetById(int id);
        IQueryable<T> GetAll();
        IQueryable<T> GetAll(Expression<Func<T, bool>> query);
        IQueryable<T> Select();
        T Save(T entity);
        IEnumerable<T> Save(List<T> entities);
        T Delete(T entityToDelete);
        T Update(T entityToUpdate);
        void Delete(IEnumerable<T> entities);
        void Dispose();
    }
}
