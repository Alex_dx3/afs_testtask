﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using TestTask.DAL.Entities;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Common;

namespace TestTask.DAL.EF
{
    public class ApplicationContext : DbContext
    {
        static ApplicationContext()
        {
            Database.SetInitializer<ApplicationContext>(new TestTaskDbInitializer());
        }

        public ApplicationContext(DbConnection connection)
        : base(connection, true)
        {
            Database.SetInitializer<ApplicationContext>(null);
        }
        public ApplicationContext(string connectionString) : base(connectionString)
        {
            Database.SetInitializer<ApplicationContext>(null);
        }
        public DbSet<TranslationRequest> Requests { get; set; }
        public DbSet<TranslationType> Translations { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

    public class TestTaskDbInitializer : DropCreateDatabaseAlways<ApplicationContext>
    {
        protected override void Seed(ApplicationContext db)
        {
            db.Translations.Add(new TranslationType { Name = "Yoda", Url = "http://api.funtranslations.com/translate/yoda.json" });
            db.Translations.Add(new TranslationType { Name = "Pirate", Url = "http://api.funtranslations.com/translate/pirate.json" });

            db.SaveChanges();
        }
    }
}
