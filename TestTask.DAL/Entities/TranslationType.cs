﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.DAL.Entities
{
    public class TranslationType : BaseEntity
    {
        public string Name { get; set; }
        public string Url { get; set; }

        public virtual ICollection<TranslationRequest> Requests { get; set; }
    }
}
