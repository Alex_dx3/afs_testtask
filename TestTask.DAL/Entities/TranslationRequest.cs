﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.DAL.Entities
{
    public class TranslationRequest : BaseEntity
    {
        public int TranslationId { get; set; }
        public string Text { get; set; }
        public string ResultText { get; set; }

        public virtual TranslationType Translation { get; set; }
    }
}
