namespace TestTask.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialize : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TranslationRequests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TranslationId = c.Int(nullable: false),
                        Text = c.String(),
                        ResultText = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TranslationTypes", t => t.TranslationId, cascadeDelete: true)
                .Index(t => t.TranslationId);
            
            CreateTable(
                "dbo.TranslationTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TranslationRequests", "TranslationId", "dbo.TranslationTypes");
            DropIndex("dbo.TranslationRequests", new[] { "TranslationId" });
            DropTable("dbo.TranslationTypes");
            DropTable("dbo.TranslationRequests");
        }
    }
}
