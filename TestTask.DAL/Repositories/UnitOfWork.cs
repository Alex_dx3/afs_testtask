﻿using TestTask.DAL.EF;
using TestTask.DAL.Entities;
using TestTask.DAL.Interfaces;
using System;
using System.Threading.Tasks;


namespace TestTask.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationContext db;

        private IRepository<TranslationType> translationRepository;
        private IRepository<TranslationRequest> requestRepository;

        public UnitOfWork(string connectionString)
        {
            db = new ApplicationContext(connectionString);
            translationRepository = new Repository<TranslationType>(db);
            requestRepository = new Repository<TranslationRequest>(db);
        }
        public UnitOfWork(ApplicationContext db)
        {
            this.db = db;
            translationRepository = new Repository<TranslationType>(db);
            requestRepository = new Repository<TranslationRequest>(db);
        }

        public IRepository<TranslationType> TranslationRepository
        {
            get
            {
                return translationRepository;
            }
        }
        public IRepository<TranslationRequest> RequestRepository
        {
            get
            {
                return requestRepository;
            }
        }
        public void Save()
        {
            db.SaveChanges();
        }
        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    translationRepository.Dispose();
                    requestRepository.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}