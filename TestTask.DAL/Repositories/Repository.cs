﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TestTask.DAL.EF;
using TestTask.DAL.Entities;
using TestTask.DAL.Interfaces;

namespace TestTask.DAL.Repositories
{
    public class Repository<T> : IDisposable, IRepository<T> where T : BaseEntity
    {
        protected readonly ApplicationContext _db;
        public Repository(ApplicationContext db)
        {
            _db = db;
        }
        public async Task<T> GetByQueryAsync(Expression<Func<T, bool>> query)
        {
            return await _db.Set<T>().FirstOrDefaultAsync(query);
        }
        public T GetById(int id)
        {
            return _db.Set<T>().Find(id);
        }
        public async Task<T> GetByIdAsync(int id)
        {
            return await _db.Set<T>().FindAsync(id);
        }
        public IQueryable<T> GetAll()
        {
            return _db.Set<T>();
        }
        public virtual IQueryable<T> GetAll(Expression<Func<T, bool>> query)
        {
            return _db.Set<T>().Where(query);
        }

        public IQueryable<T> Select()
        {
            return _db.Set<T>();
        }

        public T Save(T entity)
        {
            return _db.Set<T>().Add(entity);
        }

        public IEnumerable<T> Save(List<T> entities)
        {
            _db.Set<T>().AddRange(entities);
            return entities;
        }
        public T Detach(T entity)
        {
            _db.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        public T Delete(T entityToDelete)
        {
            if (_db.Entry(entityToDelete).State == EntityState.Detached)
            {
                _db.Set<T>().Attach(entityToDelete);
            }
            return _db.Set<T>().Remove(entityToDelete);
        }
        public T Update(T entityToUpdate)
        {
            _db.Set<T>().Attach(entityToUpdate);
            _db.Entry(entityToUpdate).State = EntityState.Modified;
            return entityToUpdate;
        }


        public void Delete(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

        public void Dispose()
        {

        }
    }
}
