﻿using Autofac;
using TestTask.BLL.Services;
using TestTask.BLL.Interface;
using TestTask.BLL.DTO;
using TestTask.DAL.Interfaces;
using AutoMapper;

namespace TestTask.WEB.Utils
{
    public class BlModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TestTask.BLL.Infrastructure.MapperProfile>();
            });
            builder.Register(c => config).As<MapperConfiguration>().SingleInstance();
            builder.Register(c => new Mapper(config)).As<IMapper>().SingleInstance();
        }
    }
}