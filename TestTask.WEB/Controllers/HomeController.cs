﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TestTask.BLL.Interface;

namespace TestTask.WEB.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITranslationService translationService;
        public HomeController(ITranslationService translationService)
        {
            this.translationService = translationService;
        }

        public async Task<ActionResult> Index()
        {
            var translationTypes = translationService.GetList();
            return View(translationTypes);
        }
    }
}