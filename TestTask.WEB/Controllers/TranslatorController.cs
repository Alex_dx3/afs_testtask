﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestTask.BLL.Interface;
using TestTask.BLL.DTO;
using System.Threading.Tasks;

namespace TestTask.WEB.Controllers
{
    public class TranslatorController : Controller
    {
        private readonly IRequestService translationRequestService;
        public TranslatorController(IRequestService translationRequestService)
        {
            this.translationRequestService = translationRequestService;
        }

        [HttpPost]
        public async Task<ActionResult> TranslateText(TranslationRequestDTO data)
        {
            var operationDetail = await translationRequestService.ExecuteRequestAsync(data);

            if (operationDetail.Succedeed)
            {
                operationDetail = translationRequestService.Create(data);
            }
            if (!operationDetail.Succedeed)
            {
                foreach (var error in operationDetail.Messages)
                {
                    ModelState.AddModelError(error.Property, error.Message);
                }
            }
            return PartialView(data);
        }
    }
}